# Oracle11GR2数据库在CentoOS环境下静默安装教程

按教程即可快速安装oracle数据库

- 系统环境版本：CentOS Linux release 7.9.2009 (其他版本没有进行测试)
- Oracle数据库版本：Oracle Database 11g Enterprise Edition Release 11.2.0.4.0 - 64bit Production

# 使用说明


#### 安装教程

1. 下载64位Oracle安装包，放到CentOS 7的/tmp目录下，包含两个文件，注意不要修改文件名称。

   链接：https://pan.baidu.com/s/1jTP34DXkSnjlqAFIaUXAXg 提取码：8888

2. 安装wget（如果已经安装，可以省略）

   > yum install wget -y

3. 下载静默安装脚本 

   > wget https://gitee.com/zhaojiyuan/oracle11g.centos7.install/raw/master/oracle11G/oracle_11g_install.sh

4. 添加脚本执行权限 

   > chmod +x oracle_11g_install.sh

5. 调用脚本，即可完成静默安装

   > ./oracle_11g_install.sh

#### 使用说明

1. 尽量用wget命令下载安装脚本，直接下载脚本可能会提示换行符不一致问题，请自行解决

2. CentOS7 需要联网互联网，用以通过Yum安装Oracle所需要的依赖

3. 目前已经完成CentOS 7的测试，可以成功安装

4. Oracle文件下载地址（百度云地址，速度你懂的）

5. 如需安装Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - 64bit Production版本的按如下操作并修改安装脚本

   1. 下载数据库安装文件，放到CentOS的/tmp目录下

      链接：https://pan.baidu.com/s/1_s7tIGDPmCJpX-MXTrbQVQ 提取码：8888

   2. 修改oracle_11g_install.sh的文件安装路径

      - oracle_file_1=/tmp/p13390677_112040_Linux-x86-64_1of7.zip**修改为**oracle_file_1=/tmp/linux.x64_11gR2_database_1of2.zip
      - oracle_file_2=/tmp/p13390677_112040_Linux-x86-64_2of7.zip**修改为**oracle_file_2=/tmp/linux.x64_11gR2_database_2of2.zip

   3. 修改版本提示（可选，没啥影响）
   
      - oracle_version="Oracle Database 11g Enterprise Edition Release 11.2.0.4.0 - 64bit Production"**修改为**oracle_version="Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - 64bit Production"
   
   4. 调用oracle_11g_install.sh脚本，开始安装